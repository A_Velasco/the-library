<?php

namespace App\Http\Controllers;

use App\Book;
use App\User;
use App\Categorie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        $books = Book::get();
        return view('book.index', compact('books', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categorie::get();
        return view('book.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try{
          $result = DB::transaction(function() use ($request){

              $book = new Book;

              $book->fill([
                  'name' => $request->name,
                  'author' => $request->author,
                  'categorie_id' => $request->category,
              ]);

              $book->save();

              DB::commit();


          });

          return redirect()->route('books');
      } catch (Exception $e){
          DB::rollBack();
          return redirect()->route('books');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(book $book)
    {
        return view('book.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        $categories = Categorie::get();
        return view('book.edit', compact('categories', 'book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try{
          $result = DB::transaction(function() use ($request, $id){

              $book = Book::find($id);

              $book->fill([
                  'name' => $request->name,
                  'author' => $request->author,
                  'categorie_id' => $request->category,
              ]);

              $book->save();

              DB::commit();


          });

          return redirect()->route('books');
      } catch (Exception $e){
          DB::rollBack();
          return redirect()->route('books');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Book::find($id)->delete();
        return json_encode(["status" => "delete"]);
    }

    public function Give_book(Request $request){
      try{
          $result = DB::transaction(function() use ($request){

              $book = Book::find($request->idbook);
              $book->status = 0;
              $book->user_id = $request->user;
              $book->save();
              DB::commit();


          });
          return redirect()->route('books');
      } catch (Exception $e){
          DB::rollBack();
          return redirect()->route('books');
      }
    }

    public function Return_book(Request $request){
      try{
          $result = DB::transaction(function() use ($request){

              $book = Book::find($request->idbook2);
              $book->status = 1;
              $book->user_id = null;
              $book->save();
              DB::commit();

          });

          return redirect()->route('books');
      } catch (Exception $e){
          DB::rollBack();
          return redirect()->route('books');
      }
    }
}
