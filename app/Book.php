<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'author', 'categorie_id',
    ];

    public function Categorie()
    {
        return $this->belongsTo('App\Categorie');
    }

    public function User()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
