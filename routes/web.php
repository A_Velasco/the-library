<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'BookController@index')->name('books');
Route::resource('books', 'BookController');
Route::post('/Give_book', 'BookController@Give_book')->name('Give_book');
Route::post('/return_book', 'BookController@Return_book')->name('return_book');
