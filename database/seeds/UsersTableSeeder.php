<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([

          'name' => "Usuario de prueba 1",
          'email' => 'Correo@prueba.com',
          'password' => bcrypt('123456'),
      ]);

      DB::table('users')->insert([

          'name' => "Usuario de prueba 2",
          'email' => 'Correo2@prueba.com',
          'password' => bcrypt('123456'),
      ]);

      DB::table('users')->insert([

          'name' => "Usuario de prueba 3",
          'email' => 'Correo3@prueba.com',
          'password' => bcrypt('123456'),
      ]);

    }
}
