<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('books')->insert([
          'name' => "Caballero de la Armadura Oxidada",
          'author' => "Robert Fisher",
          'categorie_id' => 1,
          'created_at' => '2019-06-17 11:54:20',
      ]);

      DB::table('books')->insert([
          'name' => "Cuentos de amor de locura y de muerte",
          'author' => "Horacio Quiroga",
          'categorie_id' => 1,
          'created_at' => '2019-06-17 11:54:20',
      ]);

      DB::table('books')->insert([
          'name' => "Cuentos de la selva",
          'author' => "Horacio Quiroga",
          'categorie_id' => 1,
          'created_at' => '2019-06-17 11:54:20',
      ]);

      DB::table('books')->insert([
          'name' => "El periquillo sarniento",
          'author' => "José Joaquín Fernández de Lizardi",
          'categorie_id' => 2,
          'created_at' => '2019-06-17 11:54:20',
      ]);



    }
}
