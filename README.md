## Instalacion basica

- Descargar por medio de git (git clone https://A_Velasco@bitbucket.org/A_Velasco/the-library.git)

- Bajar los paquetes necesarios de laravel  'composer update'

- Configurar el archivo .env.example segun el nombre de la base de datos, usuario y contraseña, esta se crea manualmente segun el gestor de la base de datos, cambiar el nombre del archivo y dejarlo como .env

- Correr las migrasiones con sus respectivos seeders, para crear la bd y a su vez los usuarios y libros de prueba, con el siguiente comando:
  'php artisan migrate:refresh --seed'

- Posicionar la carpeta completa dentro de un servidor virtual y apuntar directamente a la carpeta public.

## Requisitos de software
- Tener instalado Composer
- Tener un servidor web apache
- Tener instalado git
