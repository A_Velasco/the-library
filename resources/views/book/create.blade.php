@extends('layouts.primary')

@section('content')
<div class="container-fluid">
  <div class="row page-titles">
      <div class="col-12 align-self-center">
          <h4 class="text-themecolor">Add book</h4>

      </div>
  </div>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <h5>Información</h5>
                  <form action="{{route('books.store')}}" method="post" accept-charset="UTF-8">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <div class="row">

                        <div class="col-12 col-sm-4">
                              <div class="form-group">
                                  <label for="">Name<span class="requerido">*</span></label>
                                  <input type="text" class="form-control" id="name" name="name" placeholder="" required>
                              </div>
                        </div>
                        <div class="col-12 col-sm-4">
                              <div class="form-group">
                                  <label for="">Author<span class="requerido">*</span></label>
                                  <input type="text" class="form-control" id="author" name="author" placeholder="" required>
                              </div>
                        </div>

                        <div class="col-12 col-sm-4">
                              <div class="form-group">
                                  <label for="">Category</label>
                                  <select class="form-control"  name="category" required>
                                    <option value="">Seleccionar...</option>
                                    @foreach ($categories as $categorie)
                                      <option value="{{$categorie->id}}">{{$categorie->name}}</option>
                                    @endforeach

                                  </select>
                              </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mt-5 mb-4">
                        <input type="submit"class="btn btn-info" value="Add book">
                    </div>
                  </form>

              </div>
          </div>
      </div>
  </div>

</div>
@endsection
@section('modal')

@endsection
@section('script')

@endsection
