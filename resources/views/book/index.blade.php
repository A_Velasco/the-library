@extends('layouts.primary')

@section('content')

    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-8 align-self-center">
                <h4 class="text-themecolor">List of books</h4>

            </div>

        </div>
    </div>
<hr>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="table-responsive">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Author</th>
                      <th>Category</th>
                      <th>Status</th>
                      <th>Options</th>
                      <th>Published date</th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($books as $book)
                  <tr>
                    <td>{{$book->id}}</td>
                    <td>
                      <div class="media">
                        <img class="mr-3" src="/assets/img/i-gde.png" width="34" alt="Producto 1">
                        <div class="media-body">
                          {{$book->name}}
                        </div>
                      </div>
                    </td>
                    <td>{{$book->author}}</td>
                    <td>{{$book->categorie->name}}</td>
                    <td>
                      @if ($book->status == 1)
                        <span class="text-success">is available </span>
                      @else
                        <span class="text-danger">Not available </span>
                      @endif
                    </td>
                    <td>
                      <div class="d-flex justify-content-between align-items-center">
                        @if ($book->status == 1)
                          <a href="#" onclick="Give_book({{$book->id}},'{{$book->name}}')" data-toggle="modal" data-target="#mNuevoProyecto" class="acccion btn btn-link btn-sm d-lg-block m-l-15"><i class="fa fa-book"></i> <span>Give a book</span></a>
                        @elseif ($book->user_id != null)
                          <a href="#" onclick="See_user({{$book->id}},'{{$book->User->name}}')" data-toggle="modal" data-target="#user_book" class="acccion btn btn-link btn-sm d-lg-block m-l-15"><i class="fa fa-book"></i> <span>Who has the book?</span></a>
                        @endif
                        <a href="{{route('books.edit', $book->id)}}" class="acccion btn btn-link btn-sm d-lg-block m-l-15"><i class="fa fa-pencil"></i> <span>Edit book</span></a>

                        <a href="#" onclick="Delete_book({{$book->id}})" class="acccion btn btn-link btn-sm d-lg-block m-l-15"><i class="fa fa-trash-o"></i> <span>Delete</span></a>
                      </div>
                    </td>
                    <td>{{$book->created_at}}</td>
                  </tr>
                @endforeach

              </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('modal')
  <div class="modal fade" id="mNuevoProyecto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Give a book </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <form action="{{route('Give_book')}}" method="POST" accept-charset="UTF-8">
                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                          <label for="proyectoNombre">Book name<span class="requerido">*</span></label>
                          <input type="text" class="form-control" name="namebook" id="namebook" disabled>
                          <input type="hidden" name="idbook" id="idbook" >

                          <label for="cliente">Users</label>
                          <select class="form-control" name="user" required>
                            <option value="">Select...</option>
                            @foreach ($users as $user)
                              <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach

                          </select>
                          <div class="text-right mt-2">
                            <input type="submit" class="btn btn-primary btn-sm" value="Give book">
                          </div>
                      </div>

                  </form>
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="user_book" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Who has the book?</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <form action="{{route('return_book')}}" method="POST" accept-charset="UTF-8">
                   <input type="hidden" name="_token" value="{{ csrf_token() }}">

                      <div class="form-group">
                          <label for="proyectoNombre">User name<span class="requerido">*</span></label>
                          <input type="text" class="form-control" name="nameuser" id="nameuser" disabled>
                          <input type="hidden" name="idbook2" id="idbook2" >

                      </div>
                      <div class="text-right mt-2">
                        <input type="submit" class="btn btn-primary btn-sm" value="Return book">
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
@endsection
@section('script')
  <script>
    function See_user(idbook, name){
      $('#nameuser').val(name);
      $('#idbook2').val(idbook);
    }
    function Give_book(idbook, name){
      $('#namebook').val(name);
      $('#idbook').val(idbook);
    }
    function Delete_book(id){

        Swal.fire({
         title: 'Are you sure?',
         text: "You won't be able to revert this!",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
         if (result.value) {
           $.ajax({
               url:  '/books/'+id,
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               type: "DELETE",
               async: true,
               beforeSend: function() {
                   $(".loading").toggle();
               },success:  function (data) {
                 dat = JSON.parse(data);
                 console.log(dat.status);
                  if (dat.status == 'delete') {
                    Swal.fire(
                      'Deleted!',
                      'Your book has been deleted.',
                      'success'
                    )
                    window.location.href = "{{route('books')}}";
                  }
               }
           });


         }
        })

    }
      $(document).ready(function() {
          $('#example').DataTable({
              "language": {
                  "search": "Buscar",
                  "info":           "Mostrando _START_ - _END_ de _TOTAL_ libros",
                  "infoEmpty":      "Mostrando 0 de 0 de 0 entradas",
                  "lengthMenu":     "Mostrando _MENU_ registros",
                  "paginate": {
                      "first":      "Primero",
                      "last":       "Último",
                      "next":       "Siguiente",
                      "previous":   "Anterior"
                  },
              }
          });
      } );
  </script>
@endsection
