<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>The Library</title>
	<script src="/js/app.js"></script>

	<!-- Favicon icon -->
    <link rel="icon" type="/assets/image/png" sizes="16x16" href="/assets/img/favicon.png">
		<!-- jQuery library -->
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<!-- sweetalert2 -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Datatable style -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">


    <!-- Icons -->
    <link rel="stylesheet" href="/assets/css/themify-icons.css">
	<!-- Tipografía -->
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<link rel="stylesheet" href="/assets/css/style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">

</head>
<body>

<div class="preloader">
	<div class="loader">
		<div class="loader__figure"></div>
		<p class="loader__label">The Library</p>
	</div>
</div>
<div id="main-wrapper">
    <!-- Topbar header -->
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{route('books')}}">
                    <b>
                         <img src="/assets/img/i-gde.png" alt="homepage" class="light-logo">
                    </b>
                        <span class="hidden-xs" style=""><img id="logotipo" src="/assets/img/logo-hor-gde.png" alt="Logotipo"></span>
                </a>
            </div>
            <div class="navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                </ul>
                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item dropdown u-pro">
                        <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span class="hidden-md-down">¡Hola Administrador! </span>
													<img src="/assets/img/images.png" alt="user" class="">
												</a>

                    </li>
                </ul>

            </div>
	   </nav>
    </header>
    <!-- /Topbar header -->
    <!-- Menú Lateral -->
    <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar ps ps--theme_default ps--active-y" data-ps-id="2956b301-199e-ac47-8478-915e392d3a93">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav ">
                    <ul id="sidebarnav" class="in">

												<li class=""> <a class="" href="{{route('books')}}"><i class="fa fa-briefcase" aria-hidden="true"></i><span class="hide-menu">List of books</span></a>
                        </li>
												<li class=""> <a class="" href="{{route('books.create')}}"><i class="fa fa-briefcase" aria-hidden="true"></i><span class="hide-menu">Add book</span></a>
                        </li>

                    </ul>
                </nav>

            <!-- End Sidebar scroll-->
    </aside>
    <!-- /Menú Lateral -->
    <!-- Contenido -->
    <div class="page-wrapper">
			@yield('content')
    </div>
    <!-- /Contenido -->


</div>
@yield('modal')

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->

<!-- DataTable -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap Datatable -->
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- Main JS -->
<script src="/assets/js/menu-lateral.js"></script>
<script src="/assets/js/main.js"></script>
@yield('script')
</body>

</html>
